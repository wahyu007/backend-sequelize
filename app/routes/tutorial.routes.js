module.exports = app => {
    const tutorials = require("../controllers/tutorial.controller.js");

    var router = require("express").Router();

    // create a new Tutorial
    router.post("/", tutorials.create);

    // retrieve all Tutorials
    router.get('/', tutorials.findAll);

    // Retrive all published tutorials
    router.get('/published', tutorials.findAllPublished);

    //Retrive a single tutorial with id
    router.get('/:id', tutorials.findOne);

    // Update a tutorial with id
    router.put("/:id", tutorials.update);

    // Delete a tutorial with id
    router.delete('/:id', tutorials.delete);

    // Delete All Tutorial
    router.delete('/', tutorials.deleteAll);

    app.use('/api/tutorials', router)
}