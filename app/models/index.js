const dbConfig = require("../config/db.config.js");
const Sequelize = require("sequelize");

console.log(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD);

const sequelize = new Sequelize(
    dbConfig.DB, 
    dbConfig.USER, 
    dbConfig.PASSWORD, 
    {
        host : dbConfig.HOST,
        dialect : dbConfig.dialect,
        operatorsAliases: 0,
        // operatorsAliases: false,

        pool: {
            max: dbConfig.pool.max,
            min: dbConfig.pool.min,
            acquire: dbConfig.pool.acquire,
            idle: dbConfig.pool.idle
        },
        logging: true,
    });

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.tutorials = require("./tutorial.model.js")(sequelize, Sequelize);

module.exports = db;